//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// World Coordinate Record as defined in IEEE 1278.1 standard. Used to communicate the position of an entity during the simulation.
/// Typically uses WGS84 Geoid as the datum, and decimal-degrees/metres as units
pub struct WorldCoordinateRecord {
    pub x_coordinate_field: f64,
    pub y_coordinate_field: f64,
    pub z_coordinate_field: f64,
}

impl WorldCoordinateRecord {
    /// Provides a function to create a new WorldCoordinateRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a blank WorldCoordinateRecord:
    /// 
    /// ```
    /// let world_coordinate_record = WorldCoordinateRecord::new{
    ///     x: 0.0,
    ///     y: 0.0,
    ///     z: 0.0
    /// };
    /// ```
    /// 
    pub fn new(x: f64, y: f64, z: f64) -> Self {
       WorldCoordinateRecord {
           x_coordinate_field: x,
           y_coordinate_field: y,
           z_coordinate_field: z
       }
    }

    /// Fills a BytesMut struct with a WorldCoordinateRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f64(self.x_coordinate_field);
        buf.put_f64(self.y_coordinate_field);
        buf.put_f64(self.z_coordinate_field);
    }

    pub fn decode(buf: &mut BytesMut) -> WorldCoordinateRecord {
        WorldCoordinateRecord {
            x_coordinate_field: buf.get_f64(),
            y_coordinate_field: buf.get_f64(),
            z_coordinate_field: buf.get_f64(),
        }
    }
}