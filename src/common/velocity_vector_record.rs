//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// Velocity Vector Record as defined in IEEE 1278.1 standard. Used to communicate the velocity of an entity in 3D during the simulation.
/// Typically uses metres per second as units
 pub struct VelocityVectorRecord {
     pub first_vector_component_field: f32,
     pub second_vector_component_field: f32,
     pub third_vector_component_field: f32
 }

 impl VelocityVectorRecord {
    /// Provides a function to create a new VelocityVectorRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a blank VelocityVectorRecord:
    /// 
    /// ```
    /// let velocity_vector_record = VelocityVectorRecord::new{
    ///     x: 0.0,
    ///     y: 0.0,
    ///     z: 0.0
    /// };
    /// ```
    /// 
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        VelocityVectorRecord {
            first_vector_component_field: x,
            second_vector_component_field: y,
            third_vector_component_field: z
        }
     }

    /// Fills a BytesMut struct with a VelocityVectorRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f32(self.first_vector_component_field);
        buf.put_f32(self.second_vector_component_field);
        buf.put_f32(self.third_vector_component_field);
    }

    pub fn decode(buf: &mut BytesMut) -> VelocityVectorRecord {
        VelocityVectorRecord { 
            first_vector_component_field: buf.get_f32(), 
            second_vector_component_field: buf.get_f32(), 
            third_vector_component_field: buf.get_f32()
        }
    }
 }