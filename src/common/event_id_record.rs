//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use super::simulation_address_record::SimulationAddressRecord;

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// Event ID Record as defined in IEEE 1278.1 standard. Used to communicate the ID of an event during the simulation.
pub struct EventIDRecord {
    pub simulation_address_record: SimulationAddressRecord,
    pub event_identifier_field: u16
}

impl EventIDRecord {
    /// Provides a function to create a new EventIDRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a new EventIDRecord  at site 1, on application 1, with event ID 1:
    /// 
    /// ```
    /// let event_id_record = EventIDRecord::new{
    ///     site_identifier_field: 1,
    ///     application_identifier_field: 1
    ///     event_identifier_field: 1
    /// };
    /// ```
    /// 
    pub fn new(site_identifier_field: u16, application_identifier_field: u16, event_identifier_field: u16) -> Self {
        EventIDRecord {
            simulation_address_record: SimulationAddressRecord::new(site_identifier_field, application_identifier_field),
            event_identifier_field
        }
    }

    /// Provides a function to create a default EventIDRecord. Uses the default SimulationAddressRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a default EventIDRecord with event ID 2:
    /// 
    /// ```
    /// let event_id_record = EventIDRecord::default(2);
    /// ```
    /// 
    pub fn default(event_identifier: u16, ) -> Self {
        EventIDRecord {
            simulation_address_record: SimulationAddressRecord::default(),
            event_identifier_field: event_identifier
        }
    }

    /// Fills a BytesMut struct with a EventIDRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        self.simulation_address_record.serialize(buf);
        buf.put_u16(self.event_identifier_field);
    }

    pub fn decode(buf: &mut BytesMut) -> EventIDRecord {
        EventIDRecord {
            simulation_address_record: SimulationAddressRecord::decode(buf),
            event_identifier_field: buf.get_u16(),
        }
    }
}