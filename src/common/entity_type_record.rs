//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};
use num_derive::FromPrimitive;
use serde::{Serialize, Deserialize};    

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
/// Entity Type Record as defined in IEEE 1278.1 standard. Used to communicate the type of an entity during the simulation.
pub struct EntityTypeRecord {
    pub kind_field: Kind,
    pub domain_field: u8,
    pub country_field: Country,
    pub category_field: u8,
    pub subcategory_field: u8,
    pub specific_field: u8,
    pub extra_field: u8
}

impl EntityTypeRecord {
    /// Provides a function to create a new EntityTypeRecord.
    pub fn new(
        kind: Kind,
        domain: u8,
        country: Country,
        category: u8,
        subcategory: u8,
        specific: u8,
        extra: u8) -> Self {
        EntityTypeRecord {
            kind_field: kind,
            domain_field: domain,
            country_field: country,
            category_field: category,
            subcategory_field: subcategory,
            specific_field: specific,
            extra_field: extra
        }
    }

    /// Fills a BytesMut struct with a EntityTypeRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u8(self.kind_field as u8);
        buf.put_u8(self.domain_field);
        buf.put_u16(self.country_field as u16);
        buf.put_u8(self.category_field);
        buf.put_u8(self.subcategory_field);
        buf.put_u8(self.specific_field);
        buf.put_u8(self.extra_field);
    }

    pub fn decode(buf: &mut BytesMut) -> EntityTypeRecord {
        EntityTypeRecord { 
            kind_field: EntityTypeRecord::decode_kind(buf.get_u8()), 
            domain_field: buf.get_u8(), 
            country_field: EntityTypeRecord::decode_country(buf.get_u16()), 
            category_field: buf.get_u8(), 
            subcategory_field: buf.get_u8(), 
            specific_field: buf.get_u8(), 
            extra_field: buf.get_u8()
        }
    }

    fn decode_kind(data: u8) -> Kind {
        match data {
            1 => Kind::Platform,
            2 => Kind::Munition,
            3 => Kind::LifeForm,
            4 => Kind::Environmental,
            5 => Kind::CulturalFeature,
            6 => Kind::Supply,
            7 => Kind::Radio,
            8 => Kind::Expendable,
            9 => Kind::SensorEmittor,
            _ => Kind::Other
        }
    }

    fn decode_country(data: u16) -> Country {
        match data {
            1 => Country::Afghanistian,
            2 => Country::Argentina,
            3 => Country::Indonesia,
            4 => Country::Iran,
            5 => Country::Iraq,
            6 => Country::Ireland,
            7 => Country::Israel,
            8 => Country::Italy,
            9 => Country::NorthKorea,
            120 => Country::SouthKorea,
            224 => Country::UnitedKingdom,
            225 => Country::UnitedStates,
            260 => Country::Russia,
            265 => Country::Ukraine,
            _ => Country::Other
        }
    }
}

#[derive(FromPrimitive, Debug, Copy, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
/// Enum to represent the kind of entity.
pub enum Kind {
    Other = 0,
    Platform = 1,
    Munition = 2,
    LifeForm = 3,
    Environmental = 4,
    CulturalFeature = 5,
    Supply = 6,
    Radio = 7,
    Expendable = 8,
    SensorEmittor = 9
}

#[derive(FromPrimitive, Debug,Copy, Clone, PartialEq, Serialize, Deserialize)]
/// Enum to represent the country of the entity.
pub enum Country {
    Other = 0,
    Afghanistian = 1,
    Argentina = 10,
    Indonesia = 100,
    Iran = 101,
    Iraq = 102,
    Ireland = 104,
    Israel = 105,
    Italy = 106,
    Japan = 110,
    NorthKorea = 119,
    SouthKorea = 120,
    UnitedKingdom = 224,
    UnitedStates = 225,
    Russia = 260,
    Ukraine = 265,
    // ...
}