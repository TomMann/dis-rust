//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
/// Simulation Address Record as defined in IEEE 1278.1 standard. Used to communicate the ID application running during the simulation.
pub struct SimulationAddressRecord {
    pub site_identifier_field: u16,
    pub application_identifier_field: u16
}

impl SimulationAddressRecord {
    /// Provides a function to create a new SimulationAddressRecord. Enforces all IDs must be non-zero.
    /// 
    /// # Examples
    /// 
    /// Creating a new SimulationAddressRecord  at site 1, on application 1:
    /// 
    /// ```
    /// let simulation_address_record = SimulationAddressRecord::new{
    ///     site_identifier_field: 1,
    ///     application_identifier_field: 1
    /// };
    /// ```
    /// 
    pub fn new(site_identifier_field: u16, application_identifier_field: u16) -> Self {
        if site_identifier_field == 0{
            println!("Invalid site identifier field! - cannot be 0"); // TODO: Make log
        }
        if application_identifier_field == 0{
            println!("Invalid application identifier field! - cannot be 0"); // TODO: Make log
        }
        SimulationAddressRecord {
            site_identifier_field,
            application_identifier_field
        }
    }

    /// Provides a function to create a default SimulationAddressRecord.
    /// Creates a simulation address of site 1 and application 1.
    /// 
    /// # Examples
    /// 
    /// Creating a default EntityIDRecord:
    /// 
    /// ```
    /// let simulation_address_record = SimulationAddressRecord::default();
    /// ```
    /// 
    pub fn default() -> Self {
        SimulationAddressRecord {
            site_identifier_field: 1,
            application_identifier_field: 1
        }
    }
    
    /// Fills a BytesMut struct with a SimulationAddressRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u16(self.site_identifier_field);
        buf.put_u16(self.application_identifier_field);
    }

    pub fn decode(buf: &mut BytesMut) -> SimulationAddressRecord {
        SimulationAddressRecord {
            site_identifier_field: buf.get_u16(),
            application_identifier_field: buf.get_u16(),
        }
    }
}