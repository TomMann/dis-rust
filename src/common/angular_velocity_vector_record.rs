//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// Angular Velocity Record as defined in IEEE 1278.1 standard. Used to communicate the angular velocity of an entity in 3D during the simulation.
 pub struct AngularVelocityRecord {
     pub rate_about_x_axis_field: f32,
     pub rate_about_y_axis_field: f32,
     pub rate_about_z_axis_field: f32
 }

 impl AngularVelocityRecord {
    /// Provides a function to create a new AngularVelocityRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a blank AngularVelocityRecord:
    /// 
    /// ```
    /// let angular_velocity_record = AngularVelocityRecord::new{
    ///     x: 0.0,
    ///     y: 0.0,
    ///     z: 0.0
    /// };
    /// ```
    /// 
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        AngularVelocityRecord {
            rate_about_x_axis_field: x,
            rate_about_y_axis_field: y,
            rate_about_z_axis_field: z
        }
     }

    /// Fills a BytesMut struct with a AngularVelocityRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f32(self.rate_about_x_axis_field);
        buf.put_f32(self.rate_about_y_axis_field);
        buf.put_f32(self.rate_about_z_axis_field);
    }

    pub fn decode(buf: &mut BytesMut) -> AngularVelocityRecord {
        AngularVelocityRecord { 
            rate_about_x_axis_field: buf.get_f32(), 
            rate_about_y_axis_field: buf.get_f32(), 
            rate_about_z_axis_field: buf.get_f32()
        }
    }
 }