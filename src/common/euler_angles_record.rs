//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// Euler Angles Record as defined in IEEE 1278.1 standard. Used to communicate the orientation of an entity during the simulation.
/// Uses radians as units.
 pub struct EulerAnglesRecord {
     pub psi_field: f32,
     pub theta_field: f32,
     pub phi_field: f32
 }

 impl EulerAnglesRecord {
    /// Provides a function to create a new EulerAnglesRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a blank EulerAnglesRecord:
    /// 
    /// ```
    /// let euler_angles_record = EulerAnglesRecord::new{
    ///     x: 0.0,
    ///     y: 0.0,
    ///     z: 0.0
    /// };
    /// ```
    /// 
    pub fn new(psi: f32, theta: f32, phi: f32) -> Self {
        EulerAnglesRecord {
            psi_field: psi,
            theta_field: theta,
            phi_field: phi
        }
     }

    /// Fills a BytesMut struct with a EulerAnglesRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f32(self.psi_field);
        buf.put_f32(self.theta_field);
        buf.put_f32(self.phi_field);
    }

    pub fn decode(buf: &mut BytesMut) -> EulerAnglesRecord {
        EulerAnglesRecord { 
            psi_field: buf.get_f32(), 
            theta_field: buf.get_f32(), 
            phi_field: buf.get_f32()
        }
    }
 }