//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// Entity Coordinate Record as defined in IEEE 1278.1 standard. Used to communicate the position of an entity during the simulation.
 pub struct EntityCoordinateVectorRecord {
     pub x_coordinate_field: f32,
     pub y_coordinate_field: f32,
     pub z_coordinate_field: f32
 }

 impl EntityCoordinateVectorRecord {
    /// Provides a function to create a new EntityCoordinateVectorRecord.
    /// 
    /// # Examples
    /// 
    /// Creating a blank EntityCoordinateVectorRecord:
    /// 
    /// ```
    /// let entity_coordinate_record = EntityCoordinateVectorRecord::new{
    ///     x: 0.0,
    ///     y: 0.0,
    ///     z: 0.0
    /// };
    /// ```
    /// 
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        EntityCoordinateVectorRecord {
            x_coordinate_field: x,
            y_coordinate_field: y,
            z_coordinate_field: z
        }
     }

    /// Fills a BytesMut struct with a EntityCoordinateVectorRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f32(self.x_coordinate_field);
        buf.put_f32(self.y_coordinate_field);
        buf.put_f32(self.z_coordinate_field);
    }

    pub fn decode(buf: &mut BytesMut) -> EntityCoordinateVectorRecord {
        EntityCoordinateVectorRecord {
            x_coordinate_field: buf.get_f32(),
            y_coordinate_field: buf.get_f32(),
            z_coordinate_field: buf.get_f32(),
        }
    }
 }