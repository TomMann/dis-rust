//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

//! Common structs/data used throughout dis-rust
pub mod entity_coordinate_vector_record;
pub mod entity_id_record;
pub mod entity_type_record;
pub mod euler_angles_record;
pub mod event_id_record;
pub mod linear_velocity_record;
pub mod pdu_header_record;
pub mod pdu;
pub mod simulation_address_record;
pub mod velocity_vector_record;
pub mod world_coordinate_record;
pub mod linear_acceleration_record;
pub mod angular_velocity_vector_record;
pub mod dis_error;