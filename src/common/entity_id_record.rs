//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use super::simulation_address_record::SimulationAddressRecord;
use bytes::{BytesMut, BufMut, Buf};
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
/// Entity ID Record as defined in IEEE 1278.1 standard. Used to communicate the ID of an entity during the simulation.
pub struct EntityIDRecord {
    pub simulation_address_record: SimulationAddressRecord,
    pub entity_identifier_field: u16
}

impl EntityIDRecord {
    /// Provides a function to create a new EntityIDRecord. Enforces all entity IDs must be non-zero.
    /// 
    /// # Examples
    /// 
    /// Creating a new EntityIDRecord  at site 1, on application 1, with entity ID 1:
    /// 
    /// ```
    /// let entity_id_record = EntityIDRecord::new{
    ///     site_identifier_field: 1,
    ///     application_identifier_field: 1
    ///     entity_identifier_field: 1
    /// };
    /// ```
    /// 
    pub fn new(site_identifier_field: u16, application_identifier_field: u16, entity_identifier_field: u16) -> Self {
        if entity_identifier_field == 0 {
            println!("Invalid entity identifier field! - cannot be 0"); // TODO: Make this a log!
        }
        EntityIDRecord {
            simulation_address_record: SimulationAddressRecord::new(site_identifier_field, application_identifier_field),
            entity_identifier_field
        }
    }

    /// Provides a function to create a default EntityIDRecord. Uses the default SimulationAddressRecord.
    /// Enforces all entity IDs must be non-zero.
    /// 
    /// # Examples
    /// 
    /// Creating a default EntityIDRecord with event ID 2:
    /// 
    /// ```
    /// let entity_id_record = EntityIDRecord::default(2);
    /// ```
    /// 
    pub fn default(entity_identifier: u16) -> Self {
        if entity_identifier == 0 {
            println!("Invalid entity identifier field! - cannot be 0"); // TODO: Make this a log!
        }
        EntityIDRecord {
            simulation_address_record: SimulationAddressRecord::default(),
            entity_identifier_field: entity_identifier
        }
    }

    /// Fills a BytesMut struct with a EntityIDRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        self.simulation_address_record.serialize(buf);
        buf.put_u16(self.entity_identifier_field);
    }

    pub fn decode(buf: &mut BytesMut) -> EntityIDRecord {
        EntityIDRecord { 
            simulation_address_record: EntityIDRecord::decode_simulation_address(buf), 
            entity_identifier_field: buf.get_u16() 
        }
    }

    fn decode_simulation_address(buf: &mut BytesMut) -> SimulationAddressRecord {
        SimulationAddressRecord { 
            site_identifier_field: buf.get_u16(), 
            application_identifier_field: buf.get_u16()
        }
    }
}