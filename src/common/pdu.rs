//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use std::any::Any;

use bytes::BytesMut;

use super::{dis_error::DISError, pdu_header_record::PDUHeaderRecord};

/// Trait to denote a PDU.
pub trait PDU {
    /// Fills a BytesMut struct with a PDU serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    fn serialise(&self, buf: &mut BytesMut); 

    /// Creates a PDU from a BytesMut struct.
    fn deserialise(buffer: BytesMut) -> Result<Self, DISError> where Self: Sized;

    /// Creates a PDU from a BytesMut struct.
    fn deserialise_without_header(buffer: BytesMut, pdu_header: PDUHeaderRecord) -> Result<Self, DISError> where Self: Sized;

    fn as_any(&self) -> &dyn Any;
}