//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BufMut, BytesMut, Buf}; 

#[derive(Copy, Clone, Debug,)]
/// Specific Appearance Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of an entity during the simulation.
pub struct SpecificAppearanceRecord {
    pub land_platforms_record: LandPlatformsRecord,
    pub air_platforms_record: AirPlatformsRecord,
    pub surface_platforms_record: SurfacePlatformsRecord,
    pub subsurface_platforms_record: SubSurfacePlatformsRecord,
    pub space_platforms_record: SpacePlatformsRecord,
    pub guided_munitions_platforms_record: GuidedMunitionsPlatformsRecord,
    pub life_forms_record: LifeFormsRecord,
    pub environmentals_record: EnvironmentalsRecord,
}

impl SpecificAppearanceRecord {
    /// Provides a function to create a new SpecificAppearanceRecord.
    pub fn new (
        land_platforms_record:LandPlatformsRecord,
        air_platforms_record: AirPlatformsRecord,
        surface_platforms_record: SurfacePlatformsRecord,
        subsurface_platforms_record: SubSurfacePlatformsRecord,
        space_platforms_record: SpacePlatformsRecord,
        guided_munitions_platforms_record: GuidedMunitionsPlatformsRecord,
        life_forms_record: LifeFormsRecord,
        environmentals_record: EnvironmentalsRecord,) -> Self {
        SpecificAppearanceRecord {
            land_platforms_record,
            air_platforms_record,
            surface_platforms_record,
            subsurface_platforms_record,
            space_platforms_record,
            guided_munitions_platforms_record,
            life_forms_record,
            environmentals_record,
        }
    }

    /// Provides a function to create a new SpecificAppearanceRecord.
    /// The default record is for an active, powered-on entity.
    pub fn default() -> Self {
        SpecificAppearanceRecord {
            land_platforms_record: LandPlatformsRecord { 
                launcher_field: Launcher::NotRaised, 
                camouflage_type_field: CamouflageType::ForestCamoflage, 
                concealed_field: Concealed::NotConcealed, 
                unused_field: 0, 
                frozen_status_field: FrozenStatus::NotFrozen, 
                powerplant_status_field: PowerplantStatus::PowerplantOn, 
                state_field: State::Active, 
                tent_field: Tent::NotExtended, 
                ramp_field: Ramp::Up, 
                entity_specific_field: 0 },
            air_platforms_record: AirPlatformsRecord {
                afterburner_field: Afterburner::AfterburnerNotOn,
                unused_field: 0,
                frozen_status_field: FrozenStatus::NotFrozen,
                powerplant_status_field: PowerplantStatus::PowerplantOn,
                state_field: State::Active,
                entity_specific_field: 0,
            },
            surface_platforms_record: SurfacePlatformsRecord {
                unused_field: 0,
                frozen_status_field: FrozenStatus::NotFrozen,
                powerplant_status_field: PowerplantStatus::PowerplantOn,
                state_field: State::Active,
                entity_specific_field: 0,
            },
            subsurface_platforms_record: SubSurfacePlatformsRecord {
                unused_field: 0,
                frozen_status_field: FrozenStatus::NotFrozen,
                powerplant_status_field: PowerplantStatus::PowerplantOn,
                state_field: State::Active,
                entity_specific_field: 0,
            },
            space_platforms_record: SpacePlatformsRecord {
                unused_field: 0,
                frozen_status_field: FrozenStatus::NotFrozen,
                powerplant_status_field: PowerplantStatus::PowerplantOn,
                state_field: State::Active,
                entity_specific_field: 0,
            },
            guided_munitions_platforms_record: GuidedMunitionsPlatformsRecord {
                launch_flash_field: LaunchFlash::LaunchFlashPresent,
                unused_field: 0,
                frozen_status_field: FrozenStatus::NotFrozen,
                powerplant_status_field: PowerplantStatus::PowerplantOn,
                state_field: State::Active,
                entity_specific_field: 0,
            },
            life_forms_record: LifeFormsRecord {
                lifeform_state_field: LifeFormState::UprightStandingStill,
                unused_field: 0,
                frozen_status_field: FrozenStatus::NotFrozen,
                activity_state_field: State::Active,
                weapon_1_field: LifeFormWeapon::NoWeaponPresent,
                weapon_2_field: LifeFormWeapon::NoWeaponPresent,
                entity_specific_field: 0,
            },
            environmentals_record: EnvironmentalsRecord {
                density_field: Density::Clear,
                unused_field: 0,
                entity_specific_field: 0,
            },
        }
    }

    /// Fills a BytesMut struct with a SpecificAppearanceRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        self.land_platforms_record.serialize(buf);
        self.air_platforms_record.serialize(buf);
        self.surface_platforms_record.serialize(buf);
        self.subsurface_platforms_record.serialize(buf);
        self.space_platforms_record.serialize(buf);
        self.guided_munitions_platforms_record.serialize(buf);
        self.life_forms_record.serialize(buf);
        self.environmentals_record.serialize(buf);
    }

    pub fn decode(buf: &mut BytesMut) -> SpecificAppearanceRecord {
        SpecificAppearanceRecord { 
            land_platforms_record: LandPlatformsRecord::decode(buf), 
            air_platforms_record: AirPlatformsRecord::decode(buf), 
            surface_platforms_record: SurfacePlatformsRecord::decode(buf), 
            subsurface_platforms_record: SubSurfacePlatformsRecord::decode(buf), 
            space_platforms_record: SpacePlatformsRecord::decode(buf), 
            guided_munitions_platforms_record: GuidedMunitionsPlatformsRecord::decode(buf), 
            life_forms_record: LifeFormsRecord::decode(buf), 
            environmentals_record: EnvironmentalsRecord::decode(buf) 
        }
    }
}


#[derive(Debug, Clone, Copy)]
/// Land Platforms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of a land platform during the simulation.
pub struct LandPlatformsRecord {
    pub launcher_field: Launcher,
    pub camouflage_type_field: CamouflageType,
    pub concealed_field: Concealed,
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub powerplant_status_field: PowerplantStatus,
    pub state_field: State,
    pub tent_field: Tent,
    pub ramp_field: Ramp,
    pub entity_specific_field: u8,
}

impl LandPlatformsRecord {
    /// Fills a BytesMut struct with a LandPlatformsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let launcher : u16 = self.launcher_field as u16;
        let launcher = launcher << 15;
        let camouflage : u16 = self.camouflage_type_field as u16;
        let camouflage = camouflage << 13;
        let concealed : u16 = self.concealed_field as u16;
        let concealed = concealed << 12;
        let frozen_status : u16 = self.frozen_status_field as u16;
        let frozen_status = frozen_status << 10;
        let power_plant_status : u16 = self.powerplant_status_field as u16;
        let power_plant_status = power_plant_status << 9;
        let state : u16 = self.state_field as u16;
        let state = state << 8;
        let tent : u16 = self.tent_field as u16;
        let tent = tent << 7;
        let ramp : u16 = self.ramp_field as u16;
        let ramp = ramp << 6;

        let land_appearance = 0u16 | launcher | camouflage | concealed
            | frozen_status | power_plant_status | state | tent | ramp;
        buf.put_u16(land_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> LandPlatformsRecord {
        let bytes = buf.get_u16();
        LandPlatformsRecord { 
            launcher_field: Launcher::from_u8((bytes >> 1) as u8), 
            camouflage_type_field: CamouflageType::from_u8((bytes >> 2) as u8), 
            concealed_field: Concealed::from_u8((bytes >> 1) as u8), 
            unused_field: (bytes >> 1) as u8, 
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8), 
            powerplant_status_field: PowerplantStatus::from_u8((bytes >> 1) as u8), 
            state_field: State::from_u8((bytes >> 1) as u8), 
            tent_field: Tent::from_u8((bytes >> 1) as u8), 
            ramp_field: Ramp::from_u8((bytes >> 1) as u8), 
            entity_specific_field: (bytes >> 6) as u8
         }
    }
}

#[derive(Debug, Clone, Copy)]
/// Air Platforms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of an air platform during the simulation.
pub struct AirPlatformsRecord {
    pub afterburner_field: Afterburner,
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub powerplant_status_field: PowerplantStatus,
    pub state_field: State,
    pub entity_specific_field: u8,
}

impl AirPlatformsRecord {
    /// Fills a BytesMut struct with a AirPlatformsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let afterburner : u16 = self.afterburner_field as u16;
        let afterburner = afterburner << 15;
        let frozen_status : u16 = self.frozen_status_field as u16;
        let frozen_status = frozen_status << 10;
        let power_plant_status : u16 = self.powerplant_status_field as u16;
        let power_plant_status = power_plant_status << 9;
        let state : u16 = self.state_field as u16;
        let state = state << 8;

        let air_appearance : u16 = 0u16 | afterburner | frozen_status | power_plant_status | state;
        buf.put_u16(air_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> AirPlatformsRecord {
        let bytes = buf.get_u16();
        AirPlatformsRecord { 
            afterburner_field: Afterburner::from_u8((bytes >> 1) as u8), 
            unused_field: (bytes >> 4) as u8, 
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8), 
            powerplant_status_field: PowerplantStatus::from_u8((bytes >> 1) as u8), 
            state_field: State::from_u8((bytes >> 1) as u8), 
            entity_specific_field: (bytes >> 8) as u8
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Surface Platforms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of a martime surface platform during the simulation.
pub struct SurfacePlatformsRecord {
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub powerplant_status_field: PowerplantStatus,
    pub state_field: State,
    pub entity_specific_field: u8,
}

impl SurfacePlatformsRecord {
    /// Fills a BytesMut struct with a SurfacePlatformsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let frozen_status : u16 = self.frozen_status_field as u16;
        let frozen_status = frozen_status << 10;
        let power_plant_status : u16 = self.powerplant_status_field as u16;
        let power_plant_status = power_plant_status << 9;
        let state : u16 = self.state_field as u16;
        let state = state << 8;

        let surface_appearance = 0u16 | frozen_status | power_plant_status | state;
        buf.put_u16(surface_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> SurfacePlatformsRecord {
        let bytes = buf.get_u16();
        SurfacePlatformsRecord { 
            unused_field: (bytes >> 5) as u8, 
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8), 
            powerplant_status_field: PowerplantStatus::from_u8((bytes >> 1) as u8), 
            state_field: State::from_u8((bytes >> 1) as u8), 
            entity_specific_field: (bytes >> 8) as u8
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Sub-Surface Platforms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of a martime sub-surface platform during the simulation.
pub struct SubSurfacePlatformsRecord {
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub powerplant_status_field: PowerplantStatus,
    pub state_field: State,
    pub entity_specific_field: u8,
}

impl SubSurfacePlatformsRecord {
    /// Fills a BytesMut struct with a SubSurfacePlatformsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let frozen_status : u16 = self.frozen_status_field as u16;
        let frozen_status = frozen_status << 10;
        let power_plant_status : u16 = self.powerplant_status_field as u16;
        let power_plant_status = power_plant_status << 9;
        let state : u16 = self.state_field as u16;
        let state = state << 8;

        let subsurface_appearance = 0u16 | frozen_status | power_plant_status | state;
        buf.put_u16(subsurface_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> SubSurfacePlatformsRecord {
        let bytes = buf.get_u16();
        SubSurfacePlatformsRecord { 
            unused_field: (bytes >> 5) as u8, 
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8), 
            powerplant_status_field: PowerplantStatus::from_u8((bytes >> 1) as u8), 
            state_field: State::from_u8((bytes >> 1) as u8), 
            entity_specific_field: (bytes >> 8) as u8
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Space Platforms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of a space platform during the simulation.
pub struct SpacePlatformsRecord {
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub powerplant_status_field: PowerplantStatus,
    pub state_field: State,
    pub entity_specific_field: u8,
}

impl SpacePlatformsRecord {
    /// Fills a BytesMut struct with a SpacePlatformsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let frozen_status : u16 = self.frozen_status_field as u16;
        let frozen_status = frozen_status << 10;
        let power_plant_status : u16 = self.powerplant_status_field as u16;
        let power_plant_status = power_plant_status << 9;
        let state : u16 = self.state_field as u16;
        let state = state << 8;

        let subsurface_appearance = 0u16 | frozen_status | power_plant_status | state;
        buf.put_u16(subsurface_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> SpacePlatformsRecord {
        let bytes = buf.get_u16();
        SpacePlatformsRecord { 
            unused_field: (bytes >> 5) as u8, 
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8), 
            powerplant_status_field: PowerplantStatus::from_u8((bytes >> 1) as u8), 
            state_field: State::from_u8((bytes >> 1) as u8), 
            entity_specific_field: (bytes >> 8) as u8
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Guided Munitions Platforms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of a guided munition platform during the simulation.
pub struct GuidedMunitionsPlatformsRecord {
    pub launch_flash_field: LaunchFlash,
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub powerplant_status_field: PowerplantStatus,
    pub state_field: State,
    pub entity_specific_field: u8,
}

impl GuidedMunitionsPlatformsRecord {
    /// Fills a BytesMut struct with a GuidedMunitionsPlatformsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let launch_flash : u16 = self.launch_flash_field as u16;
        let frozen_status : u16 = self.frozen_status_field as u16;
        let state : u16 = self.state_field as u16;

        let guided_appearance = 0u16
            | (launch_flash << 15)
            | (frozen_status << 10)
            | (state << 8);
        buf.put_u16(guided_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> GuidedMunitionsPlatformsRecord {
        let bytes = buf.get_u16();
        GuidedMunitionsPlatformsRecord {
            launch_flash_field: LaunchFlash::from_u8((bytes >> 1) as u8),
            unused_field: (bytes >> 4) as u8,
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8),
            powerplant_status_field: PowerplantStatus::from_u8((bytes >> 1) as u8),
            state_field: State::from_u8((bytes >> 1) as u8),
            entity_specific_field: (bytes >> 8) as u8,
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Life Forms Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of a life form (human) during the simulation.
pub struct LifeFormsRecord {
    pub lifeform_state_field: LifeFormState,
    pub unused_field: u8,
    pub frozen_status_field: FrozenStatus,
    pub activity_state_field: State,
    pub weapon_1_field: LifeFormWeapon,
    pub weapon_2_field: LifeFormWeapon,
    pub entity_specific_field: u8,
}

impl LifeFormsRecord {
    /// Fills a BytesMut struct with a LifeFormsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let life_form_state : u16 = self.lifeform_state_field as u16;
        let frozen_status : u16 = self.frozen_status_field as u16;
        let activity_state : u16 = self.activity_state_field as u16;
        let weapon_1 : u16 = self.weapon_1_field as u16;
        let weapon_2 : u16 = self.weapon_2_field as u16;

        let life_form_appearance = 0u16
            | (life_form_state << 12)
            | (frozen_status << 10)
            | (activity_state << 8)
            | (weapon_1 << 6)
            | (weapon_2 << 4);
        buf.put_u16(life_form_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> LifeFormsRecord {
        let bytes = buf.get_u16();
        LifeFormsRecord {
            lifeform_state_field: LifeFormState::from_u8((bytes >> 4) as u8),
            unused_field: (bytes >> 1) as u8,
            frozen_status_field: FrozenStatus::from_u8((bytes >> 1) as u8),
            activity_state_field: State::from_u8((bytes >> 1) as u8),
            weapon_1_field: LifeFormWeapon::from_u8((bytes >> 2) as u8),
            weapon_2_field: LifeFormWeapon::from_u8((bytes >> 2) as u8),
            entity_specific_field: (bytes >> 4) as u8,
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Environmentals Record as defined in IEEE 1278.1 standard. Used to communicate the specific appearance of environmental effects during the simulation.
pub struct EnvironmentalsRecord {
    pub density_field: Density,
    pub unused_field: u8,
    pub entity_specific_field: u8,
}

impl EnvironmentalsRecord {
    /// Fills a BytesMut struct with a EnvironmentalsRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let density : u16 = self.density_field as u16;
        let env_appearance = 0u16 | (density << 12);
        buf.put_u16(env_appearance);
    }

    pub fn decode(buf: &mut BytesMut) -> EnvironmentalsRecord {
        let bytes = buf.get_u16();
        EnvironmentalsRecord {
            density_field: Density::from_u8((bytes >> 4) as u8),
            unused_field: (bytes >> 4) as u8,
            entity_specific_field: (bytes >> 8) as u8,
            
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent a launcher's appearance.
pub enum Launcher {
    NotRaised = 0,
    Raised = 1,
}

impl Launcher {
    pub fn from_u8(bit: u8) -> Launcher {
        match bit {
            0 => Launcher::NotRaised,
            1 => Launcher::Raised,
            2_u8..=u8::MAX => Launcher::NotRaised   
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent a camouflage's appearance.
pub enum CamouflageType {
    DesertCamouflage = 0,
    WinterCamoflage = 1,
    ForestCamoflage = 2,
    Unused = 3,
}

impl CamouflageType {
    pub fn from_u8(bit: u8) -> CamouflageType {
        match bit {
            0 => CamouflageType::DesertCamouflage,
            1 => CamouflageType::WinterCamoflage,
            2 => CamouflageType::ForestCamoflage,
            3 => CamouflageType::Unused,
            2_u8..=u8::MAX => CamouflageType::Unused         
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's concealment status.
pub enum Concealed {
    NotConcealed = 0,
    EntityConcealed = 1,
}

impl Concealed {
    pub fn from_u8(bit: u8) -> Concealed {
        match bit {
            0 => Concealed::NotConcealed,
            1 => Concealed::EntityConcealed,
            2_u8..=u8::MAX => Concealed::NotConcealed          
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's frozen status. A frozen entity does need updates to it's rendering.
pub enum FrozenStatus {
    NotFrozen = 0,
    Frozen = 1,
}

impl FrozenStatus {
    pub fn from_u8(bit: u8) -> FrozenStatus {
        match bit {
            0 => FrozenStatus::NotFrozen,
            1 => FrozenStatus::Frozen,
            2_u8..=u8::MAX => FrozenStatus::NotFrozen           
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's power plant status.
pub enum PowerplantStatus {
    PowerplantOff = 0,
    PowerplantOn = 1,
}

impl PowerplantStatus {
    pub fn from_u8(bit: u8) -> PowerplantStatus {
        match bit {
            0 => PowerplantStatus::PowerplantOff,
            1 => PowerplantStatus::PowerplantOn,
            2_u8..=u8::MAX => PowerplantStatus::PowerplantOff          
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's activation status.
pub enum State {
    Active = 0,
    Deactivated = 1,
}

impl State {
    pub fn from_u8(bit: u8) -> State {
        match bit {
            0 => State::Active,
            1 => State::Deactivated,
            2_u8..=u8::MAX => State::Active         
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's tent status.
pub enum Tent {
    NotExtended = 0,
    Extended = 1,
}

impl Tent {
    pub fn from_u8(bit: u8) -> Tent {
        match bit {
            0 => Tent::NotExtended,
            1 => Tent::Extended,
            2_u8..=u8::MAX => Tent::NotExtended        
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's ramp status.
pub enum Ramp {
    Up = 0,
    Down = 1,
}

impl Ramp {
    pub fn from_u8(bit: u8) -> Ramp {
        match bit {
            0 => Ramp::Up,
            1 => Ramp::Down,
            2_u8..=u8::MAX => Ramp::Up   
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's afterburner status.
pub enum Afterburner {
    AfterburnerNotOn = 0,
    AfterburnerOn = 1,
}

impl Afterburner {
    pub fn from_u8(bit: u8) -> Afterburner {
        match bit {
            0 => Afterburner::AfterburnerNotOn,
            1 => Afterburner::AfterburnerOn,
            2_u8..=u8::MAX => Afterburner::AfterburnerNotOn         
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an entity's launch flash status.
pub enum LaunchFlash {
    NoLaunchFlashPresent = 0,
    LaunchFlashPresent = 1,
}

impl LaunchFlash {
    pub fn from_u8(bit: u8) -> LaunchFlash {
        match bit {
            0 => LaunchFlash::NoLaunchFlashPresent,
            1 => LaunchFlash::LaunchFlashPresent,
            2_u8..=u8::MAX => LaunchFlash::NoLaunchFlashPresent        
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent a lifeform's status.
pub enum LifeFormState {
    Null = 0,
    UprightStandingStill = 1,
    UprightWalking = 2,
    UprightRunning = 3,
    Kneeling = 4,
    Prone = 5,
    Crawling = 6,
    Swimming = 7,
    Parachuting = 8,
    Jumping = 9,
}

impl LifeFormState {
    pub fn from_u8(bit: u8) -> LifeFormState {
        match bit {
            0 => LifeFormState::Null,
            1 => LifeFormState::UprightStandingStill,
            2 => LifeFormState::UprightWalking,
            3 => LifeFormState::UprightRunning,
            4 => LifeFormState::Kneeling,
            5 => LifeFormState::Prone,
            6 => LifeFormState::Crawling,
            7 => LifeFormState::Swimming,
            8 => LifeFormState::Parachuting,
            9 => LifeFormState::Jumping,
            2_u8..=u8::MAX => LifeFormState::Null       
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent a lifeform's weapon status.
pub enum LifeFormWeapon {
    NoWeaponPresent = 0,
    WeaponIsStowed = 1,
    WeaponIsDeployed = 2,
    WeaponInFiringPositon = 3,
}

impl LifeFormWeapon {
    pub fn from_u8(bit: u8) -> LifeFormWeapon {
        match bit {
            0 => LifeFormWeapon::NoWeaponPresent,
            1 => LifeFormWeapon::WeaponIsStowed,
            2 => LifeFormWeapon::WeaponIsDeployed,
            3 => LifeFormWeapon::WeaponInFiringPositon,
            2_u8..=u8::MAX => LifeFormWeapon::NoWeaponPresent     
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent an environmental effect's density.
pub enum Density {
    Clear = 0,
    Hazy = 1,
    Dense = 2,
    VeryDense = 3,
    Opaque = 4,
}

impl Density {
    pub fn from_u8(bit: u8) -> Density {
        match bit {
            0 => Density::Clear,
            1 => Density::Hazy,
            2 => Density::Dense,
            3 => Density::VeryDense,
            4 => Density::Opaque,
            2_u8..=u8::MAX => Density::Clear        
        }
    }
}