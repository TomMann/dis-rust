//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};
use num_derive::FromPrimitive;

use crate::common::{linear_acceleration_record::LinearAccelerationRecord, angular_velocity_vector_record::AngularVelocityRecord}; 

#[derive(Copy, Clone, Debug,)]
/// Dead Reckoning Parameters Record as defined in IEEE 1278.1 standard. Used to communicate the dead reckoning algorithm used by an entity during the simulation.
pub struct DeadReckoningParametersRecord {
    pub dead_reckoning_algorithm_field: DeadReckoningAlgorithm,
    pub dead_reckoning_other_parameters_field: u8,
    pub entity_linear_acceleration_record: LinearAccelerationRecord,
    pub entity_angular_velocity_record: AngularVelocityRecord,
}

impl DeadReckoningParametersRecord {
    /// Provides a function to create a new DeadReckoningParametersRecord.
    pub fn new(
        dead_reckoning_algorithm_field: DeadReckoningAlgorithm,
        entity_linear_acceleration_record: LinearAccelerationRecord,
        entity_angular_velocity_record: AngularVelocityRecord,) -> Self {
        DeadReckoningParametersRecord {
            dead_reckoning_algorithm_field,
            dead_reckoning_other_parameters_field: 0,
            entity_linear_acceleration_record,
            entity_angular_velocity_record
        }
    }

    /// Provides a function to create a default DeadReckoningParametersRecord for a static entity.
    pub fn default() -> Self {
        DeadReckoningParametersRecord {
            dead_reckoning_algorithm_field: DeadReckoningAlgorithm::Static,
            dead_reckoning_other_parameters_field: 0,
            entity_linear_acceleration_record: LinearAccelerationRecord::new(0.0, 0.0, 0.0),
            entity_angular_velocity_record: AngularVelocityRecord::new(0.0, 0.0, 0.0),
        }
    }

    /// Fills a BytesMut struct with a DeadReckoningParametersRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u8(self.dead_reckoning_algorithm_field as u8);
        buf.put_bytes(0u8, 15);
        self.entity_linear_acceleration_record.serialize(buf);
        self.entity_angular_velocity_record.serialize(buf);
    }

    pub fn decode(buf: &mut BytesMut) -> DeadReckoningParametersRecord {
        DeadReckoningParametersRecord {
            dead_reckoning_algorithm_field: DeadReckoningAlgorithm::from_u8(buf.get_u8()),
            dead_reckoning_other_parameters_field: 0,
            entity_linear_acceleration_record: LinearAccelerationRecord::decode(buf),
            entity_angular_velocity_record: AngularVelocityRecord::decode(buf),
        }
    }
}

#[derive(Debug, FromPrimitive, PartialEq, Copy, Clone)]
/// Enum to capture the dead reckoning algorithm used by an entity.
pub enum DeadReckoningAlgorithm {
    Other = 0,
    Static = 1,
    DRMFPW = 2,
    DRMRPW = 3,
    DRMRVW = 4,
    DRMFVW = 5,
    DRMFPB = 6,
    DRMRPB = 7,
    DRMRVB = 8,
    DRMFVB = 9,
}

impl DeadReckoningAlgorithm {
    pub fn from_u8(bit: u8) -> DeadReckoningAlgorithm {
        match bit {
            0 => DeadReckoningAlgorithm::Other,
            1 => DeadReckoningAlgorithm::Static,
            2 => DeadReckoningAlgorithm::DRMFPW,
            3 => DeadReckoningAlgorithm::DRMRPW,
            4 => DeadReckoningAlgorithm::DRMRVW,
            5 => DeadReckoningAlgorithm::DRMFVW,
            6 => DeadReckoningAlgorithm::DRMFPB,
            7 => DeadReckoningAlgorithm::DRMRPB,
            8 => DeadReckoningAlgorithm::DRMRVB,
            9 => DeadReckoningAlgorithm::DRMFVB,
            2_u8..=u8::MAX => DeadReckoningAlgorithm::Other
            
        }
    }
}