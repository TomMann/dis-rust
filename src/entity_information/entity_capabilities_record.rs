//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Copy, Clone, Debug, Default)]
/// Entity Capabilities Record as defined in IEEE 1278.1 standard. Used to communicate the capabilities of an entity during the simulation.
pub struct EntityCapabilitiesRecord {
    pub ammunition_supply_field: bool,
    pub fuel_supply_field: bool,
    pub recovery_field: bool,
    pub repair_field: bool,
}

impl EntityCapabilitiesRecord {
    /// Provides a function to create a new EntityCapabilitiesRecord.
    pub fn new(
        ammunition_supply_field: bool,
        fuel_supply_field: bool,
        recovery_field: bool,
        repair_field: bool,) -> Self {
        EntityCapabilitiesRecord {
            ammunition_supply_field,
            fuel_supply_field,
            recovery_field,
            repair_field,
        }
    }

    /// Provides a function to create a default EntityCapabilitiesRecord 
    /// representing an entity with no capabilities.
    pub fn default() -> Self {
        EntityCapabilitiesRecord {
            ammunition_supply_field: false,
            fuel_supply_field: false,
            recovery_field: false,
            repair_field: false,
        }
    }

    /// Fills a BytesMut struct with a EntityCapabilitiesRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        let ammunition_supply = if self.ammunition_supply_field { 1u32 } else { 0u32 } << 31;
        let fuel_supply = if self.fuel_supply_field { 1u32 } else { 0u32 } << 30;
        let recovery = if self.recovery_field { 1u32 } else { 0u32 } << 29;
        let repair = if self.repair_field { 1u32 } else { 0u32 } << 28;
        let capabilities = 0u32 | ammunition_supply | fuel_supply | recovery | repair;
        buf.put_u32(capabilities);
    }

    pub fn decode(buf: &mut BytesMut) -> EntityCapabilitiesRecord {
        let bytes = buf.get_u32();
        EntityCapabilitiesRecord {
            ammunition_supply_field: (bytes >> 1) != 0,
            fuel_supply_field: (bytes >> 1) != 0,
            recovery_field: (bytes >> 1) != 0,
            repair_field: (bytes >> 1) != 0,
        }
    }
}