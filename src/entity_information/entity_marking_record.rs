//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::{BytesMut, BufMut, Buf};

#[derive(Clone, Debug,)]
/// Entity Marking Record as defined in IEEE 1278.1 standard. Used to communicate any markings on an entity during the simulation.
pub struct EntityMarkingRecord {
    pub entity_marking_character_set_field: EntityMarkingCharacterSet,
    pub entity_marking_string_record: String,
}

impl EntityMarkingRecord {
    /// Provides a function to create a new EntityMarkingRecord.
    pub fn new(
        entity_marking_character_set_field: EntityMarkingCharacterSet,
        entity_marking_string_record: String,) -> Self {
        EntityMarkingRecord {
            entity_marking_character_set_field,
            entity_marking_string_record
        }
    }

    /// Provides a function to create a default EntityMarkingRecord.
    /// Provides an Entity Marking Record with a string and ASCII encoding.
    /// 
    /// # Examples
    /// 
    /// Creating a default EntityMarkingRecord with TEST as the marking:
    /// 
    /// ```
    /// let entity_marking_record = EntityMarkingRecord::default("TEST".to_string());
    /// ```
    /// 
    pub fn default(marking: String) -> Self {
        EntityMarkingRecord {
            entity_marking_character_set_field: EntityMarkingCharacterSet::ASCII,
            entity_marking_string_record: marking
        }
    }

    /// Fills a BytesMut struct with a EntityMarkingRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u8(self.entity_marking_character_set_field as u8);
        let marking = self.entity_marking_string_record.clone();
        buf.put_slice(&marking.into_bytes()[..]);
    }

    pub fn decode(buf: &mut BytesMut) -> EntityMarkingRecord {
        EntityMarkingRecord {
            entity_marking_character_set_field: EntityMarkingCharacterSet::from_u8(buf.get_u8()),
            entity_marking_string_record: "".to_string(),
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Enum to represent the character set used by the marking.
pub enum EntityMarkingCharacterSet {
    Unused = 0,
    ASCII = 1,
    ArmyMarking = 2,
    DigitChevron = 3,
}

impl EntityMarkingCharacterSet {
    pub fn from_u8(bit: u8) -> EntityMarkingCharacterSet {
        match bit {
            0 => EntityMarkingCharacterSet::Unused,
            1 => EntityMarkingCharacterSet::ASCII,
            2 => EntityMarkingCharacterSet::ArmyMarking,
            3 => EntityMarkingCharacterSet::DigitChevron,  
            4_u8..=u8::MAX => EntityMarkingCharacterSet::Unused          
        }
    }
}