//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

//! PDUs/data belonging to the Entity Information PDU family
pub mod entity_appearance_record;
pub mod entity_state_pdu;
pub mod general_appearance_record;
pub mod specific_appearance_record;
pub mod dead_reckoning_parameters_record;
pub mod entity_marking_record;
pub mod entity_capabilities_record;