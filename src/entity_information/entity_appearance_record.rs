//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use bytes::BytesMut;

use super::{general_appearance_record::GeneralAppearanceRecord, specific_appearance_record::SpecificAppearanceRecord};

#[derive(Copy, Clone, Debug,)]
/// Entity MArking Record as defined in IEEE 1278.1 standard. Used to communicate the appearance an entity during the simulation.
pub struct EntityAppearanceRecord {
    pub general_appearance_record: GeneralAppearanceRecord,
    pub specific_appearance_record: SpecificAppearanceRecord
}

impl EntityAppearanceRecord {
    /// Provides a function to create a new EntityAppearanceRecord.
    pub fn new(general_appearance_record: GeneralAppearanceRecord, specific_appearance_record: SpecificAppearanceRecord) -> Self {
        EntityAppearanceRecord {
            general_appearance_record,
            specific_appearance_record
        }
    }

    /// Provides a function to create a default EntityAppearanceRecord using the default generic/specific appearance records.
    pub fn default() -> Self {
        EntityAppearanceRecord {
            general_appearance_record: GeneralAppearanceRecord::default(),
            specific_appearance_record: SpecificAppearanceRecord::default()
        }
    }

    /// Fills a BytesMut struct with a EntityAppearanceRecord serialised into binary. This buffer is then ready to be sent via
    /// UDP to the simluation network.
    pub fn serialize(&self, buf: &mut BytesMut) {
        self.general_appearance_record.serialize(buf);
        self.specific_appearance_record.serialize(buf);
    }

    pub fn decode(buf: &mut BytesMut) -> EntityAppearanceRecord {
        EntityAppearanceRecord { 
            general_appearance_record: GeneralAppearanceRecord::decode(buf), 
            specific_appearance_record: SpecificAppearanceRecord::decode(buf) 
        }
    }
}