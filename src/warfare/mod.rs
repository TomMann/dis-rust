//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).


//! PDUs/data belonging to the Warfare PDU family
pub mod fire_pdu;
pub mod detonation_pdu;
pub mod burst_descriptor_record;