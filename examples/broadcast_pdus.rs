//     dis-rust - A rust implementation of the DIS simulation protocol.
//     Copyright (C) 2022 Thomas Mann
// 
//     This software is dual-licensed. It is available under the conditions of
//     the GNU Affero General Public License (see the LICENSE file included) 
//     or under a commercial license (email contact@coffeebreakdevs.com for
//     details).

use std::{net::UdpSocket};

use bytes::BytesMut;

extern crate dis_rust;
use dis_rust::{warfare::{fire_pdu::FirePDU, detonation_pdu::DetonationPDU}, entity_information::entity_state_pdu::EntityStatePDU, simulation_management::event_report_pdu::EventReportPDU, common::pdu::PDU};

const SRC_PORT: u16 = 3000;

fn main() {
    let src_addr = format!("192.168.1.134:{}", SRC_PORT);
    println!("binding to {} for sending", src_addr.as_str());
    let socket = UdpSocket::bind(src_addr).expect("bind should succeed");

    socket.set_broadcast(true).expect("set_broadcast to true should succeed");

    // Send FirePDU
    let data_to_send = FirePDU::default();
    println!("broadcasting data: {:?}", data_to_send);
    let mut buffer = BytesMut::new();
    PDU::serialise(&data_to_send, &mut buffer);
    socket
        .send_to(&buffer, format!("255.255.255.255:{}", SRC_PORT))
        .expect("couldn't send data");

    // Send EventReportPDU
    let data_to_send = EventReportPDU::default();
    println!("broadcasting data: {:?}", data_to_send);
    let mut buffer = BytesMut::new();
    PDU::serialise(&data_to_send, &mut buffer);
    socket
        .send_to(&buffer, format!("255.255.255.255:{}", SRC_PORT))
        .expect("couldn't send data");


    // Send EntityStatePDU
    let data_to_send = EntityStatePDU::default();
    println!("broadcasting data: {:?}", data_to_send);
    let mut buffer = BytesMut::new();
    PDU::serialise(&data_to_send, &mut buffer);
    socket
        .send_to(&buffer, format!("255.255.255.255:{}", SRC_PORT))
        .expect("couldn't send data");

    // Send DetonationPDU
    let data_to_send = DetonationPDU::default();
    println!("broadcasting data: {:?}", data_to_send);
    let mut buffer = BytesMut::new();
    PDU::serialise(&data_to_send, &mut buffer);
    socket
        .send_to(&buffer, format!("255.255.255.255:{}", SRC_PORT))
        .expect("couldn't send data");
}