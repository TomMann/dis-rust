# DIS-rust

[![forthebadge](https://forthebadge.com/images/badges/made-with-rust.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)

![forthebadge](https://img.shields.io/docsrs/dis-rust?style=for-the-badge)
![forthebadge](https://img.shields.io/maintenance/yes/2022?style=for-the-badge)
![forthebadge](https://img.shields.io/crates/d/dist-rust?style=for-the-badge)

![forthebadge](https://img.shields.io/crates/l/dis-rust?style=for-the-badge)
![forthebadge](https://img.shields.io/crates/v/dis-rust?style=for-the-badge)

A rust implementation of the DIS simulation protocol.

## Getting Started

Introductions to DIS can be found here:

- [Integrating the DIS Standards Into a Fully-Immersive Simulation Application](http://open-dis.org/searis_paper40-1.pdf)
- [DIS Plain and Simple: an Introduction to DIS](https://www.google.ca/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwjQs8HsoPvVAhVi4IMKHZHyC_cQFggoMAA&url=https%3A%2F%2Fwww.sisostds.org%2FDigitalLibrary.aspx%3FCommand%3DCore_Download%26EntryId%3D29302&usg=AFQjCNHbhiBCVmEDrTaWZBD2tFUWKg4yVw)

Currently supported DIS PDUs are:

- [Entity State PDU](https://faculty.nps.edu/brutzman/vrtp/mil/navy/nps/disEnumerations/JdbeHtmlFiles/pdu/29.htm)
- [Fire PDU](https://faculty.nps.edu/brutzman/vrtp/mil/navy/nps/disEnumerations/JdbeHtmlFiles/pdu/7c.htm)
- [Detonation PDU](https://faculty.nps.edu/brutzman/vrtp/mil/navy/nps/disEnumerations/JdbeHtmlFiles/pdu/84.htm)
- [Event Report PDU](https://faculty.nps.edu/brutzman/vrtp/mil/navy/nps/disEnumerations/JdbeHtmlFiles/pdu/b3.htm)

Note that it is up to the application to decode simulation-specific enumerations. A good starting point are [the SISO enumerations](https://www.sisostds.org/ProductsPublications/ReferenceDocuments.aspx).

### Prerequisites

Requirements for the software and other tools to build, test and push:

- [rust](https://www.rust-lang.org/carg)
- [cargo](https://doc.rust-lang.org/cargo/)
- [clippy](https://github.com/rust-lang/rust-clippy)

### Installing

The easiest way to include this crate in your project is to simply add the
 following to your Cargo.toml file:

```toml
dis-rust = "0.2.10"
```

### Documentation

Further documentation can be found on [docs.rs](https://docs.rs/dis-rust/latest/dis-rust/).

## Running the tests

Unit tests can be run using cargo:

```bash
cargo test
```

### Style tests

Clippy will complain about the linting within dis-rust due the DIS enumerations.

Rustc will complain about the DIS procotol version enumerations in the PDU header record.
These warnings have been disabled in-line within the source code.

## Versioning

We use [Semantic Versioning](http://semver.org/) for versioning. For the versions
available, see the [tags on this
repository](https://gitlab.com/TomMann/dis-rust).

## Authors

- **Thomas Mann** - *Original Author* -
    [tom.mann@coffeebreakdevs.co.uk](mailto:tom.mann@coffeebreakdevs.co.uk)

## License / Copyright

This project is dual licensed under the [AGPLv3.0](LICENSE)
License - see the [LICENSE](LICENSE) file for
details.

It is also available under a commerical license. Please email
[contact@coffeebreakdevs.co.uk](mailto:contact@coffeebreakdevs.co.uk) for more details.

Copyright (C) 2022 Thomas Mann
